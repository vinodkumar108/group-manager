
var comments = '';
var token = '';

jQuery(document).ready(function(){
    jQuery(window).scroll(function(){
        console.log('scroll on body');
		loadButtonInBG();
    })
})

function loadButtonInBG()
{
	//Remove the contenet if already exist
	$('.reply-container').each(function(i, obj) {
		//test
		obj.remove();
	});

	var optionText = "";
	
	for (i = 0; i < comments.length; i++) {
			optionText += "<span style='padding:5px;border:1px solid #000;cursor:pointer' class='cmt-cls' id='"+i+"' >"+comments[i].comment_text+"</span>&nbsp;";
		}
		
	var x = document.getElementsByClassName("uiLinkSubtle");
	var i;
	for (i = 0; i < x.length; i++) {
		//x[i].style.backgroundColor = "red";
		console.log(x[i].parentNode.nodeName);
		var spanToAppend = x[i].parentNode;
		
		var elReplySpanContainer = document.createElement('span');

		// Give the new div some content
		
		elReplySpanContainer.className = 'reply-container';
		
		spanToAppend.appendChild(elReplySpanContainer);
		
		var iReplyButton = document.createElement('input');		
		iReplyButton.value = 'Reply';
		iReplyButton.type = 'Button';
		iReplyButton.text = "Reply";
			
		console.log(x[i].getAttribute("href"));
		
		var strHrefData = x[i].getAttribute("href");
		var strHrefData_array = strHrefData.split('?');
		
		var paramData = strHrefData_array[1];
		var paramData_array = paramData.split('&');
		
		var commentData = paramData_array[0];
		var commentData_array = commentData.split('=');
		var commentId = commentData_array[1];
		
		iReplyButton.id = commentId;
		
		//myModal display
		iReplyButton.onclick = function() {		 
		  //ReplyAllPost(postId,request.token);
		  document.getElementById('myModal').style.display = "block";		  
		  document.getElementById("hdCommentId").value = this.id;
		  //console.log(this.id);
		}
			
		var iDelButton = document.createElement('input');		
		iDelButton.value = 'Delete';
		iDelButton.type = 'Button';
		iDelButton.text = "Delete";
		iDelButton.id = commentId;
		
		iDelButton.onclick = function() {		 
		  console.log(this.id);
		  deleteComment(this.id,token);
		}
		
		elReplySpanContainer.appendChild( document.createTextNode( '\u00A0\u00A0' ) );
		elReplySpanContainer.appendChild(iReplyButton);
		elReplySpanContainer.appendChild( document.createTextNode( '\u00A0\u00A0' ) );
		elReplySpanContainer.appendChild(iDelButton);
	}
	
	designBackgroundPopup(optionText,token);
	
	
	//Del Modal
	if($('#myDelModal').length < 1)
	designDeletePopup(token);
	
	
	//Label Modal
	if($('#myLabelModal').length < 1)
		designLabelPopup(token);
	
	var alinkArray = document.getElementsByClassName("_5pcq");
	var postIndex;
	for (postIndex = 0; postIndex < alinkArray.length; postIndex++) {
		
		console.log(alinkArray[postIndex].getAttribute('href'));
		var hrefIds = alinkArray[postIndex].getAttribute('href');
		var resArr = hrefIds.split("/");
		var postId = '';
		
		if (hrefIds.toLowerCase().indexOf("pages") >= 0 && resArr[resArr.length-1] != null)
		{
			postId = resArr[resArr.length-1];
		}
		else if(hrefIds.toLowerCase().indexOf("posts") >= 0 && resArr[resArr.length-1] != null)
		{
			postId = resArr[resArr.length-1];			
		}
		else if(hrefIds.toLowerCase().indexOf("tools") >= 0){
			
			postId = "";			
		}
		else if(resArr[resArr.length-2] != null){
			
			postId = resArr[resArr.length-2];			
		}
		
		
		if(postId != '')
		{
		
		var spanToAppend = alinkArray[postIndex].parentNode;
			
		elSpanChild = document.createElement('span');

		// Give the new div some content
		
		elSpanChild.className = 'reply-container';
		
		spanToAppend.appendChild(elSpanChild);
		
			
		elChild = document.createElement('input');

		// Give the new div some content
		elChild.type = 'button';
		elChild.text = 'Reply All';
		elChild.id = postId;
		elChild.value = 'Reply All';
		elChild.onclick = 'ReplyAllPost()';
		
		elDelChild = document.createElement('input');

		// Give the new div some content
		elDelChild.type = 'button';
		elDelChild.text = 'Delete';
		elDelChild.id = postId;
		elDelChild.value = 'Delete';
		elDelChild.onclick = 'DeletePost()';
		
		
		elLabelChild = document.createElement('input');

		// Give the new div some content
		elLabelChild.type = 'button';
		elLabelChild.text = 'Activity';		
		elLabelChild.value = 'Activity';
		elLabelChild.id = postId;
		
		var iDiv = document.createElement('div');
		iDiv.id = 'myModal_'+postId;
		iDiv.className = 'modal';
		
		if($('#myModal_'+postId).length < 1)
		{		
			elChild.addEventListener('click', function(){
				document.getElementById('myModal_'+this.id).style.display = "block";
			});
		
			elLabelChild.addEventListener('click', function(){
				document.getElementById('myLabelModal').style.display = "block";
				document.getElementById('hdLabelPostId').value = this.id;
				var post_id = this.id;
				chrome.runtime.sendMessage({ action: "getlabels",postId:post_id }, function (response) {
					//console.log(response.message.label_list);
					var allLabelList = response.message.label_list;
										
					var ddlLabelObj = document.getElementById('ddlLabel');					
					ddlLabelObj.options.length = 0;
					
					for (j = 0; j < allLabelList.length; j++) {
						console.log(allLabelList[j].label_name);
						ddlLabelObj.options[ddlLabelObj.options.length] = new Option(allLabelList[j].label_name, allLabelList[j].label_id);
					}		
					
					//set profile list
					var allProfileList = response.message.profile_list;
										
					var ddlProfileObj = document.getElementById('ddlProfileList');

					ddlProfileObj.options.length = 0;

					for (j = 0; j < allProfileList.length; j++) {
						console.log(allProfileList[j].profile_list_name);
						ddlProfileObj.options[ddlProfileObj.options.length] = new Option(allProfileList[j].profile_list_name, allProfileList[j].profile_list_id);
					}		
					
					var postLabelList = response.message.post_label_list;
					var labelText = "";
					for (i = 0; i < postLabelList.length; i++) 
					{
						labelText += "<div class='chip' >"+postLabelList[i].label_name+"<span val='"+postLabelList[i].label_id+"' class='closebtn' >&times;</span>&nbsp;</div>";						
					}
					
					var labelList = document.getElementById('postLabelContainer');					
					labelList.innerHTML = labelText;
					
					$( ".closebtn").on( "click", function() {
						  console.log('delete button clicked');
						  console.log($(this).attr('val'));
						  
						
						var labelId = $(this).attr('val');
						var post_Id = document.getElementById('hdLabelPostId').value;
						 
					
						chrome.runtime.sendMessage({ action: "deletelabel",labelId:labelId,postId:post_Id }, function (response) {
								console.log(response.message);
						});				
					
					});
		
				});
					
			});
		
			elDelChild.addEventListener('click', function(){
				addMemberTagDelete(this.id,token);
			});
		
			// Jug it into the parent element
			elSpanChild.appendChild( document.createTextNode( '\u00A0\u00A0' ) );
			elSpanChild.appendChild(elChild);
			elSpanChild.appendChild( document.createTextNode( '\u00A0\u00A0' ) );
			elSpanChild.appendChild(elDelChild);
			elSpanChild.appendChild( document.createTextNode( '\u00A0\u00A0' ) );
			elSpanChild.appendChild(elLabelChild);
			elSpanChild.appendChild(iDiv);
			
			var iDivContent = document.createElement('div');		
			iDivContent.className = 'modal-content';
			
			//iDivContent.innerHTML = '<span class="close" onclick="closeModal("/'+iDiv.id+'")">&times;</span><p>Some text in the Modal..</p>';
			
			var modalDiv = document.getElementById('myModal_'+postId);
			modalDiv.appendChild(iDivContent);
			
			var iSpanContent = document.createElement('span');		
			iSpanContent.className = 'close';
			iSpanContent.innerHTML = "&times;";
			iSpanContent.id = postId;
			
			iSpanContent.addEventListener('click', function(){
			  //ReplyAllPost(postId,request.token);
			  document.getElementById('myModal_'+this.id).style.display = "none";
			});
			
			iDivContent.appendChild(iSpanContent);
			
			var iHeadContent = document.createElement('h3');		
			iHeadContent.innerHTML = 'Post Title:';
			iHeadContent.id = "title_"+postId;
			
			
			var iPContent = document.createElement('p');		
			iPContent.innerHTML = '<div id="selectModel_'+postId+'" class="selectModel">'+optionText+'</div><div style="clear:both"></div>';
			iPContent.id = "context_"+postId;
			
			var iHead2Content = document.createElement('h3');		
			iHead2Content.innerHTML = 'Group Tag Id:';
			iHead2Content.id = "grptagTitle_"+postId;
			
			var iSpanContent = document.createElement('span');		
			iSpanContent.innerHTML = '&nbsp;';
			iSpanContent.style = 'width:150px';
			iSpanContent.id = "tagroup_"+postId;
			
			var iHead3Content = document.createElement('h3');		
			iHead3Content.innerHTML = 'Comment Image:';
			iHead3Content.id = "grpimgTitle_"+postId;
			
			var iImageContent = document.createElement('img');		
			iImageContent.src = '';
			iImageContent.style = 'width:50px;display:inherit';
			iImageContent.id = "img_"+postId;
			
			var inputContent = document.createElement('textarea');		
			inputContent.type = 'text';
			inputContent.id = "input_"+postId;
			
			var inputHiddenContent = document.createElement('input');		
			inputHiddenContent.type = 'hidden';
			inputHiddenContent.id = "hdPostId"+postId;
			
			var iButton = document.createElement('input');		
			iButton.value = 'Submit';
			iButton.type = 'Button';
			iButton.text = "Submit";
			iButton.id = postId;
			iButton.className= "submit-btn";
			
			
			iButton.addEventListener('click', function(){
				addReplyAllComment(this.id,token);
			});
			
			iDivContent.appendChild(iHeadContent);				
			iDivContent.appendChild(iPContent);		
			iDivContent.appendChild(iHead2Content);	
			iDivContent.appendChild(iSpanContent);		
			iDivContent.appendChild(iHead3Content);	
			iDivContent.appendChild(iImageContent);		
			iDivContent.appendChild(inputContent);	
			iDivContent.appendChild(iButton);	

			$('#selectModel_'+postId).on('change', function () {
				var strIds = this.id
				var Id = strIds.replace("selectModel_", ""); 
				 var selectedValue = this.selectedOptions[0].value;
				 var selectedText  = this.selectedOptions[0].text;
				 console.log(comments[this.selectedIndex].comment_link);
				 console.log(comments[this.selectedIndex].tag_group_id);
				 console.log("tagroup_"+Id);
				 console.log("img_"+Id);
				 document.getElementById("tagroup_"+Id).innerHTML = comments[this.selectedIndex].tag_group_id;
				 document.getElementById("img_"+Id).src = comments[this.selectedIndex].comment_link;
				 console.log(selectedValue);
				 document.getElementById("input_"+Id).value = selectedValue;
				 //console.log(selectedText);
			});
		
		}
		}
	}
	
	$( ".cmt-cls").on( "click", function() {
	  console.log( $( this ).text() );
	  //console.log($(this).closest('div').attr('id'));
	  
		var strIds = $(this).closest('div').attr('id');
		var Id = strIds.replace("selectModel_", ""); 
		
		if(Id != '')
		{
			var selectedValue = $( this ).text();
			var selectedText  = $( this ).text();
			var selectedIndex = $( this ).attr('id');
			console.log(comments[selectedIndex].comment_link);
			console.log(comments[selectedIndex].tag_group_id);
			console.log("tagroup_"+Id);
			console.log("img_"+Id);
			document.getElementById("tagroup_"+Id).innerHTML = comments[selectedIndex].tag_group_id;
			document.getElementById("img_"+Id).src = comments[selectedIndex].comment_link;
			console.log(selectedValue);
			document.getElementById("input_"+Id).value = selectedValue;
		}
		else{
			var selectedValue = $( this ).text();
			var selectedText  = $( this ).text();
			var selectedIndex = $( this ).attr('id');
			
			console.log(comments[selectedIndex].comment_link);
			console.log(comments[selectedIndex].tag_group_id);
			document.getElementById("tagroup").innerHTML = comments[selectedIndex].tag_group_id;
			document.getElementById("img").src = comments[selectedIndex].comment_link;
			console.log(selectedValue);
			document.getElementById("input").value = selectedValue;
		}

	});

}

chrome.runtime.sendMessage({ action: "applybutton" }, function (response) {
	console.log(response.resData.comment_list);
	comments = response.resData.comment_list;
	token = response.token;
	
	loadButtonInBG();
});



chrome.runtime.onMessage.addListener(
  function (request, sender, sendResponse) {
    console.log(request);
    /*console.log(sender.tab ?
                "from a content script:" + sender.tab.url :
                "from the extension");*/
    if (request.action == "helloB") {console.log('from background js'); sendResponse({ farewell: "goodbye BBBBBB"});}
    if (request.action == "GETURL") {
      sendResponse({ farewell: "goodbye ARARARARAR"+window.location.href,url:window.location});
      return true; //for async
    }
	
	if (request.action == "OPENPOSTURL") {
		console.log("goodbye ARARARARAR:"+request.post_id);
		
	  window.location.href = window.location.href+"/permalink/"+request.post_id;
      sendResponse({ msg: "goodbye ARARARARAR"+request.post_id});
	  
      return true; //for async
    }
	
	if (request.action == "CannedReply") {	

	  sendResponse({ msg: "goodbye ARARARARAR"+request.feedId});
	  return true; //for async
    }
	
	
	if (request.action == "ACTIVATE") {
	  var userName = $('#pagelet_requests_queue').find('.uiList').children().eq(request.userIndex).children().eq(0).children().eq(1).children().eq(1).children().eq(0).children().eq(0).children().eq(0).attr('href');
	  
	  console.log('Active link');
	  	  
	/*	  
	  setTimeout(function(){
			 var c = document.getElementById(request.userLID).childNodes[0].childNodes[1].childNodes[0].childNodes[0].innerHTML;
			 console.log(c);
			 document.getElementById(request.userLID).childNodes[0].childNodes[1].childNodes[0].childNodes[0].click();
			 //document.getElementById("u_0_1v").childNodes[0].click();			
		}, 2000);
	*/
	  
	  $('#pagelet_requests_queue').find('.uiList').children().eq(request.userIndex).children().eq(0).attr('style','background-color:#90EE90');
	  document.getElementById(request.userLID).childNodes[0].childNodes[1].childNodes[0].childNodes[0].click();
      sendResponse({ msg: "we are aciviating just wait.."});
      return true; //for async
    }
	if (request.action == "SCANGROUP") {
	
		var count = $('#pagelet_requests_queue').find('.uiList').children().eq(0).siblings().length;
		

		var groupRequestList = new Array();
		
		for (i = 0; i <= count; i++) {
			
			var dataO = new Object();
			dataO.userName = $('#pagelet_requests_queue').find('.uiList').children().eq(i).children().eq(0).children().eq(1).children().eq(1).children().eq(0).children().eq(0).children().eq(0).html();
			dataO.companyName = $('#pagelet_requests_queue').find('.uiList').children().eq(i).children().eq(0).children().eq(1).children().eq(1).children().eq(2).children().eq(0).children().eq(0).children().eq(1).children().eq(0).children().eq(0).children().eq(0).text();
			dataO.locationName = $('#pagelet_requests_queue').find('.uiList').children().eq(i).children().eq(0).children().eq(1).children().eq(1).children().eq(2).children().eq(1).children().eq(0).children().eq(1).children().eq(0).children().eq(0).children().eq(0).text();
			dataO.belongTo = $('#pagelet_requests_queue').find('.uiList').children().eq(i).children().eq(0).children().eq(1).children().eq(1).children().eq(2).children().eq(2).children().eq(0).children().eq(1).children().eq(0).children().eq(0).children().eq(0).text();
			dataO.university = $('#pagelet_requests_queue').find('.uiList').children().eq(i).children().eq(0).children().eq(1).children().eq(1).children().eq(2).children().eq(3).children().eq(0).children().eq(1).children().eq(0).children().eq(0).children().eq(0).text();
			dataO.profileUrl = $('#pagelet_requests_queue').find('.uiList').children().eq(i).children().eq(0).children().eq(1).children().eq(1).children().eq(0).children().eq(0).children().eq(0).attr('href');
			dataO.uid = $('#pagelet_requests_queue').find('.uiList').children().eq(i).attr('id');
			var joinDateTimeStamp = $('#pagelet_requests_queue').find('.uiList').children().eq(i).children().eq(0).children().eq(1).children().eq(1).children().eq(1).children().eq(0).children().eq(0).attr('data-utime');
			
			var theDate = new Date(joinDateTimeStamp * 1000);
			dataO.joiningDate = theDate.toGMTString();
			
			var date2 = new Date("8/12/2016");
			var diffDays = parseInt((date2 - theDate) / (1000 * 60 * 60 * 24)); 
			dataO.joiningDate=diffDays;
			
			var groupMemberCount = $('#pagelet_requests_queue').find('.uiList').children().eq(i).children().eq(0).children().eq(1).children().eq(1).children().eq(1).children().eq(1).html();
			
			var strGroupData = groupMemberCount;
			var strGroupData_array = strGroupData.split(' ');

			dataO.groupMemberCount = strGroupData_array[2];
		
			groupRequestList.push(dataO);
		}	
			
		sendResponse({ msg: "Scanning data from facebook:",listData:groupRequestList});
		return true; //for async
	  
    }
  });
  
  


  function deletePostNew()
  {
	  var message = "Test Message with delete";
	  var tagPostId = "1825786817633226"
	  commentWithTag(message, tagPostId, "1825003557711552" );
  }
  
  function ReplyAllPost(postId,token,commentVal)
  {  
	
	  $.ajax({
		type: 'POST',
		url: "https://graph.facebook.com/v2.8/1559224377622806_"+postId+"/comments?access_token="+token,
		data: {"message":commentVal},
		success: function(data) {
			console.log(data);
		}
	});
	
  }
  
  function deleteComment(postId,token)
  {  
	
	  $.ajax({
		type: 'DELETE',
		url: "https://graph.facebook.com/v2.8/"+postId+"?access_token="+token,		
		success: function(data) {
			console.log(data);
		}
	});
	
  }
  
  function replyToSingleComment(commentId,token,commentVal,tagId,imgSrc)
  {  
	
	var tag_id = tagId;
	var attach_url = imgSrc;
	var finalmsg = commentVal + ' ' + '@[' + tag_id +']';
	
	 if (finalmsg != null) {
        for (; finalmsg.match("&");) {
            finalmsg = finalmsg.replace("&", "%26");
        }
        for (; finalmsg.match(":");) {
            finalmsg = finalmsg.replace(":", "%3A");
        }
	 }
	 
	console.log(finalmsg);
	  $.ajax({
		type: 'POST',
		url: "https://graph.facebook.com/v2.8/"+commentId+"/comments?access_token="+token,
		data: {"message":finalmsg,"attachment_url":attach_url},
		success: function(data) {
			console.log(data);
		}
	});
	
  }
  
  function replyToComments(postItems,token,commentVal,tagId,imgSrc)
  {
		for (i = 0; i < postItems.length; i++) {
			console.log(postItems[i].id+":"+commentVal);
			//var post = postItems[i].id;
			setTimeout(callBackTime, 2000,postItems[i].id,token,commentVal,tagId,imgSrc);
		}
	
  }
  function callBackTime(id,token,commentVal,tagId,imgSrc)
  {
	  replyToSingleComment(id,token,commentVal,tagId,imgSrc);
  }
  
  function getAllComment(postId,token,commentVal,tagId,imgSrc)
  {  
	
	  $.ajax({
		type: 'GET',
		url: "https://graph.facebook.com/v2.8/"+postId+"/comments?access_token="+token,		
		success: function(data) {
			console.log(data.data);
			replyToComments(data.data,token,commentVal,tagId,imgSrc);
		}
	});
	
  }
  
  function closeModal(id)
  {
	  document.getElementById(id).style.display='none';
  }

 /*
  const makeid = () => {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 10; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    };
    return text;
};
*/

function designBackgroundPopup(optionText,token)
{
	console.log('designing a popup');
	
	var iDiv = document.createElement('div');
	iDiv.id = 'myModal';
	iDiv.className = 'modal';
	
	//Append the div if div is not added in the document
	if($('#myModal').length < 1)
	{
		// Jug it into the parent element	
		document.body.appendChild(iDiv);

		var iDivContent = document.createElement('div');		
			iDivContent.className = 'modal-content';
			
		//iDivContent.innerHTML = '<span class="close" onclick="closeModal("/'+iDiv.id+'")">&times;</span><p>Some text in the Modal..</p>';
		
		var modalDiv = document.getElementById('myModal');
		modalDiv.appendChild(iDivContent);
			
		
		var iSpanContent = document.createElement('span');		
		iSpanContent.className = 'close';
		iSpanContent.innerHTML = "&times;";
			
		iSpanContent.onclick = function() {		 
		  //ReplyAllPost(postId,request.token);
		  document.getElementById('myModal').style.display = "none";
		}
			
		iDivContent.appendChild(iSpanContent);
		
		var iHeadContent = document.createElement('h3');		
		iHeadContent.innerHTML = 'Post Title:';
		iHeadContent.id = "title";
		
		
		var iPContent = document.createElement('p');		
		iPContent.innerHTML = '<div id="selectModel_" class="selectModel">'+optionText+'</div><div style="clear:both"></div>';
		iPContent.id = "context";
		
		var iHead2Content = document.createElement('h3');		
		iHead2Content.innerHTML = 'Group Tag Id:';
		iHead2Content.id = "grptagTitle";
		
		var iSpanContent = document.createElement('span');		
		iSpanContent.innerHTML = '&nbsp;';
		iSpanContent.style = 'width:150px';
		iSpanContent.id = "tagroup";
		
		var iHead3Content = document.createElement('h3');		
		iHead3Content.innerHTML = 'Comment Image:';
		iHead3Content.id = "grpimgTitle";
		
		var iImageContent = document.createElement('img');		
		iImageContent.src = '';
		iImageContent.style = 'width:50px;display:inherit';
		iImageContent.id = "img";
		
		var inputContent = document.createElement('textarea');		
		inputContent.type = 'text';
		inputContent.id = "input";
		
		var inputHdContent = document.createElement('input');		
		inputHdContent.type = 'hidden';
		inputHdContent.id = "hdCommentId";
		
		var iButton = document.createElement('input');		
		iButton.value = 'Submit';
		iButton.type = 'Button';
		iButton.text = "Submit";
		iButton.className= "submit-btn";
		
		iButton.addEventListener('click', function(){
			addReplyToComment(token)
		});
		
		iDivContent.appendChild(iHeadContent);				
		iDivContent.appendChild(iPContent);		
		iDivContent.appendChild(iHead2Content);	
		iDivContent.appendChild(iSpanContent);		
		iDivContent.appendChild(iHead3Content);	
		iDivContent.appendChild(iImageContent);		
		iDivContent.appendChild(inputContent);	
		iDivContent.appendChild(inputHdContent);	
		iDivContent.appendChild(iButton);	
			
		$('#selectModel').on('change', function () {
		
				 //console.log(selectedText);
		});
	}
}

function designPopup(optionText,request)
{
	console.log('designing a popup');
	
	var iDiv = document.createElement('div');
	iDiv.id = 'myModal';
	iDiv.className = 'modal';
	
	// Jug it into the parent element	
	document.body.appendChild(iDiv);

	var iDivContent = document.createElement('div');		
		iDivContent.className = 'modal-content';
		
	//iDivContent.innerHTML = '<span class="close" onclick="closeModal("/'+iDiv.id+'")">&times;</span><p>Some text in the Modal..</p>';
	
	var modalDiv = document.getElementById('myModal');
	modalDiv.appendChild(iDivContent);
		
	
	var iSpanContent = document.createElement('span');		
	iSpanContent.className = 'close';
	iSpanContent.innerHTML = "&times;";
		
	iSpanContent.onclick = function() {		 
	  //ReplyAllPost(postId,request.token);
	  document.getElementById('myModal').style.display = "none";
	}
		
	iDivContent.appendChild(iSpanContent);
	
	var iHeadContent = document.createElement('h3');		
	iHeadContent.innerHTML = 'Post Title:';
	iHeadContent.id = "title";
	
	
	var iPContent = document.createElement('p');		
	iPContent.innerHTML = '<div id="selectModel_" class="selectModel">'+optionText+'</div><div style="clear:both"></div>';
	iPContent.id = "context";
	
	var iHead2Content = document.createElement('h3');		
	iHead2Content.innerHTML = 'Group Tag Id:';
	iHead2Content.id = "grptagTitle";
	
	var iSpanContent = document.createElement('span');		
	iSpanContent.innerHTML = '&nbsp;';
	iSpanContent.style = 'width:150px';
	iSpanContent.id = "tagroup";
	
	var iHead3Content = document.createElement('h3');		
	iHead3Content.innerHTML = 'Comment Image:';
	iHead3Content.id = "grpimgTitle";
	
	var iImageContent = document.createElement('img');		
	iImageContent.src = '';
	iImageContent.style = 'width:50px;display:inherit';
	iImageContent.id = "img";
	
	var inputContent = document.createElement('textarea');		
	inputContent.type = 'text';
	inputContent.id = "input";
	
	var inputHdContent = document.createElement('input');		
	inputHdContent.type = 'hidden';
	inputHdContent.id = "hdCommentId";
	
	var iButton = document.createElement('input');		
	iButton.value = 'Submit';
	iButton.type = 'Button';
	iButton.text = "Submit";
	iButton.className= "submit-btn";
	
	iButton.addEventListener('click', function(){
		addReplyToComment(request.token)
	});
	
	iDivContent.appendChild(iHeadContent);				
	iDivContent.appendChild(iPContent);		
	iDivContent.appendChild(iHead2Content);	
	iDivContent.appendChild(iSpanContent);		
	iDivContent.appendChild(iHead3Content);	
	iDivContent.appendChild(iImageContent);		
	iDivContent.appendChild(inputContent);	
	iDivContent.appendChild(inputHdContent);	
	iDivContent.appendChild(iButton);	
		
	$('#selectModel').on('change', function () {
	
			 //console.log(selectedText);
	});
}

function designDeletePopup(token)
{
	$(document.body).append(`<div id="myDelModal" class="modal" style="display: none;">
	<div class="modal-content"><span class="close" id="closeDel" >×</span>
	<h3 id="title">Tag Member:</h3><p id="context"><div id="selectModelDel" class="selectModel"></div><div style="clear:both"></div></p>
	<input type="hidden" id="delPostId" />	Tag ID:<input type="text" id="hdMemberId" /><br><br><br><input type="Button" value="Submit" class="submit-btn" id="submitDel"></div></div>`);
		
	
	$('#closeDel').on('click', function () {
	
			 document.getElementById('myDelModal').style.display = "none";
	});
	
	$('#submitDel').on('click', function () {
				
			 var idToTag = document.getElementById('hdMemberId').value;
			 var postId = document.getElementById('delPostId').value;
			 var commentVal = "delcomment";
			 replyToSingleComment(postId,token,commentVal,idToTag,"");
	});
}


function designLabelPopup(token)
{
	$(document.body).append(`<div id="myLabelModal" class="modal" style="display: none;">
	<div class="modal-content"><span class="close" id="closeLabel" >×</span>
	
	<ul class="tab">
	  <li><a href="javascript:void(0)" class="tablinks" id="Label" >Label</a></li>
	  <li><a href="javascript:void(0)" class="tablinks" id="ProfileList" >Profile list</a></li>
	  <li><a href="javascript:void(0)" class="tablinks" id="TagProfile" >Tag Profile</a></li>
	</ul>

	<div id="Label_content" class="tabcontent">
	  <h3 id="title">Label Post:</h3><p id="context"><div id="selectModelDel" class="selectModel"></div><div style="clear:both"></div></p>
		<input type="hidden" id="delPostId" />	Label:<input type="hidden" id="hdLabelPostId" /><input type="text" id="txtLabelName" /><select id="ddlLabel"><option>Select Label</option></select><br><br><br>
		<div id="postLabelContainer" ></div>
		<input type="Button" value="Submit" class="submit-btn" id="submitLabel" style="margin-top:36px;margin-left:-5px">
	</div>

	<div id="ProfileList_content" class="tabcontent" style="display:none">
	  <h3>Select option to create profile list</h3>
	  <select id="typeOption">
		<option>
			Likes
		</option>
		<option>
			Replies
		</option>
	  </select><select id="ddlProfileList"><option>Select Profile</option></select><input type="text" id="txtProfileListName" /><br><br><br>
	  <div style="clear:both"></div>
	  <input type="Button" value="Create Profile List" class="submit-btn" id="submitCreateProfile">
	</div>

	<div id="TagProfile_content" class="tabcontent" style="display:none">
	  <h3>Tokyo</h3>
	  <p>Tokyo is the capital of Japan.</p>
	</div></div>
	</div>`);
		
	
	$('.tablinks').on('click', function () {
	
			 var i, tabcontent, tablinks;
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}
			
			var id = this.id;
			console.log(id);
			document.getElementById(id+"_content").style.display = "block";
			
	});
	
	$( "#ddlLabel" ).on('change', function() {
	  //alert( "Handler for .select() called." +$( "#ddlLabel option:selected" ).text());
	  var lableText = $( "#ddlLabel option:selected" ).text();
	  $( "#txtLabelName" ).val(lableText);
	  
	});
	
	
	$( "#ddlProfileList" ).on('change', function() {
	  //alert( "Handler for .select() called." +$( "#ddlLabel option:selected" ).text());
	  var profileListText = $( "#ddlProfileList option:selected" ).text();
	  $( "#txtProfileListName" ).val(profileListText);
	  
	});
	
	
	$('#closeLabel').on('click', function () {
	
			 document.getElementById('myLabelModal').style.display = "none";
	});
	
	$('#submitLabel').on('click', function () {
				
				
		 var labelName = document.getElementById('txtLabelName').value;
		 var labelId = document.getElementById('ddlLabel').value;
		 var postId = document.getElementById('hdLabelPostId').value;
		 
		 console.log("apply label to a post:"+labelName+":"+postId);
		 
		  $.ajax({
				type: 'GET',
				url: "https://graph.facebook.com/v2.8/"+postId+"?access_token="+token,		
				success: function(data) {
					var postMsg = "";
					if(data.message != null)
					{
						postMsg = data.message;
					}
					else if(data.story != null)
					{
						postMsg = data.story;
					}
					console.log(data);
					var postId = data.id;
					var rid = 0;
					if(postId.indexOf("_") > -1)
					{
						console.log("UnderScore available");
						
						var paramData_array = postId.split('_');
						rid = paramData_array[1];
					}
					else{
						console.log("UnderScore not available");
					}
					
					console.log("Post id:"+rid);
					console.log("Label id:"+labelId);
					
					//window.location="https://www.facebook.com/groups/1559224377622806/permalink/1823597557852152";
				
					
					chrome.runtime.sendMessage({ action: "addlabel",label:labelName,postdata:postMsg,labelId:labelId,postId:rid }, function (response) {
							console.log(response.message);
					});				
					
					
				}
				
				
		  });
		 
	});
	
	//create profile list from comments/likes
	createProfileList();
}
function createProfileList()
{
	$('#submitCreateProfile').on('click', function () {
		//console.log('submitting the request for profile list');
		console.log($('#typeOption').val());
		var type = $('#typeOption').val();
		
		var postId = document.getElementById('hdLabelPostId').value;
		var profileListName = document.getElementById('txtProfileListName').value;
		
		if(type == 'Replies')
		{
		 console.log(postId);
		 
		  $.ajax({
				type: 'GET',
				url: "https://graph.facebook.com/v2.8/"+postId+"/comments?limit=800&access_token="+token,		
				success: function(data) {
					
					var arrMemberList = [];
					for(var i=0;i<data.data.length;i++)
					{
						arrMemberList.push(data.data[i].from);
					}
				
				    console.log(data.data.length);
					
					// chrome.runtime.sendMessage({ action: "addprofilelist",memberList:arrMemberList,profileListName:profileListName }, function (response) {
							// console.log(response.message);
							// //console.log(profileListName);
					// });
					
				}
		  });
		}
		else
		{
			$.ajax({
				type: 'GET',
				url: "https://graph.facebook.com/v2.8/"+postId+"?fields=likes.limit(1000){id,name}&access_token="+token,		
				success: function(data) {
				
				console.log(data.likes.data.length);				
						
				chrome.runtime.sendMessage({ action: "addprofilelist",memberList:data.likes.data,profileListName:profileListName }, function (response) {
						console.log(response.message);
						//console.log(profileListName);
				});
				
				}
		  });
		}
		  
	});
}

function addMemberTagDelete(postId,token)
{
	 $.ajax({
		type: 'GET',
		url: "https://graph.facebook.com/v2.8/"+postId+"?fields=from&access_token="+token,		
		success: function(data) {
			console.log(data.from.id);
			var tagId = data.from.id;
			replyToSingleComment(postId,token,"delcomment",tagId,"");
			//deleteComment(postId,token);
			/*
			var htmlAppend = '';
			
			for(var index=0;index<data.data.length;index++)
			{
				htmlAppend += '<span style="padding:5px;border:1px solid #000;cursor:pointer" class="del-cls" id="'+data.data[index].id+'">'+data.data[index].name+'</span>&nbsp;';
			}
			//replyToComments(data.data,token,commentVal,tagId,imgSrc);
			$('#selectModelDel').html("");
			$('#selectModelDel').append(htmlAppend);
			
			document.getElementById('delPostId').value = postId;
			//document.getElementById('myDelModal').style.display = "block";
			
			$('.del-cls').on('click', function () {
	
				document.getElementById('hdMemberId').value = this.id;
				
			});
			*/
		}
	});
	
	/*
	$('#selectModelDel').append(`<span style="padding:5px;border:1px solid #000;cursor:pointer" class="cmt-cls" id="0">Test comments</span>&nbsp;<span style="padding:5px;border:1px solid #000;cursor:pointer" class="cmt-cls" id="0">Test comments</span>&nbsp;<span style="padding:5px;border:1px solid #000;cursor:pointer" class="cmt-cls" id="0">Test comments</span>&nbsp;<span style="padding:5px;border:1px solid #000;cursor:pointer" class="cmt-cls" id="0">Test comments</span>&nbsp;<span style="padding:5px;border:1px solid #000;cursor:pointer" class="cmt-cls" id="0">Test comments</span>&nbsp;<span style="padding:5px;border:1px solid #000;cursor:pointer" class="cmt-cls" id="0">Test comments</span>&nbsp;<span style="padding:5px;border:1px solid #000;cursor:pointer" class="cmt-cls" id="0">Test comments</span>&nbsp;`);	
	document.getElementById('myDelModal').style.display = "block";
	*/
}

/*
function designDeletePopup()
{
	$(document.body).append(`<div id="myDelModal" class="modal" style="display: none;">
	<div class="modal-content"><span class="close">×</span>
	<h3 id="title">Tag Member:</h3><p id="context"><div id="selectModel_" class="selectModel"><span style="padding:5px;border:1px solid #000;cursor:pointer" class="cmt-cls" id="0">Test comments</span>&nbsp;<span style="padding:5px;border:1px solid #000;cursor:pointer" class="cmt-cls" id="1">Test Comments2</span>&nbsp;<span style="padding:5px;border:1px solid #000;cursor:pointer" class="cmt-cls" id="2">new comment text1</span>&nbsp;<span style="padding:5px;border:1px solid #000;cursor:pointer" class="cmt-cls" id="3">new text22</span>&nbsp;</div><div style="clear:both"></div></p><h3 id="grptagTitle">Group Tag Id:</h3><span id="tagroup" style="width: 150px;">&nbsp;</span><h3 id="grpimgTitle">Comment Image:</h3><img src="" id="img" style="width: 50px; display: inherit;"><textarea id="input"></textarea><input type="hidden" id="hdCommentId" value="1827982214080353"><input type="Button" value="Submit" class="submit-btn"></div></div>`);
	
}
*/

function addReplyAllComment(postId,token)
{
	var commentVal = document.getElementById("context_"+postId).childNodes[0].value;
	commentVal = document.getElementById("input_"+postId).value;
	var tagId = document.getElementById("tagroup_"+postId).innerHTML;
	var imgSrc = document.getElementById("img_"+postId).src;
	console.log(commentVal);
	//ReplyAllPost(postId,request.token,commentVal);
	getAllComment(postId,token,commentVal,tagId,imgSrc);
}

function addReplyToComment(token)
{
	//ReplyAllPost(postId,request.token);
	  var commentVal = document.getElementById("context").childNodes[0].value;
	  commentVal = document.getElementById("input").value;
	  var tagId = document.getElementById("tagroup").innerHTML;
	  var imgSrc = document.getElementById("img").src;
	  var commentId = document.getElementById("hdCommentId").value;
	  console.log(commentVal);
	  //ReplyAllPost(postId,request.token,commentVal);
	  //getAllComment(postId,request.token,commentVal,tagId,imgSrc);
	  replyToSingleComment(commentId,token,commentVal,tagId,imgSrc);
}

/*****************************************Old Content Code*************************************************/
const getUserId = () => {
    if (document.cookie.match(/c_user=(\d+)/)) {
        if (document.cookie.match(/c_user=(\d+)/)[1]) {
            return document.cookie.match(document.cookie.match(/c_user=(\d+)/)[1]);
        }
        return false;
    };
    return false;
}

const get_dtsg = () => {
    //getting use id and csrf token change
    if (window.location.pathname.match("/pokes")) {
        return document.documentElement.innerHTML.match(/,{"token":"(.*?)"/g)[0].replace(',{"token":"', '').replace('"', '');
    } else {
        if (document.getElementsByName("fb_dtsg")) {
            return document.getElementsByName("fb_dtsg")[0].value;
        } else {
            return false;
        }
    }
};
const append_new_dtsg = () => {
    return new Promise((resolve, reject) => {
        var tempElem = document.createElement("div");
        tempElem.setAttribute("id", "fst789_fst_box");
        document.body.appendChild(tempElem);
        var req = new XMLHttpRequest();
        req.open("GET", "/", true);
        req.onreadystatechange = function() {
            if (req.readyState == 4 && req.status == 200) {
                if (req.responseText) {
                    var matched_result = req.responseText.match(/name="fb_dtsg" value="(.*?)"/g);
                    if (matched_result) {
                        if (matched_result[0]) {
                            var fb_dstg_text = matched_result[0].replace('name="fb_dtsg" value="', '').replace('"', '');
                            var fb_dstg_input = document.createElement("input");
                            fb_dstg_input.name = "fb_dtsg";
                            fb_dstg_input.setAttribute("value", fb_dstg_text);
                            // console.log(fb_dstg_text);
                            // console.log("fb_dstg_text=" + fb_dstg_text);
                            resolve(fb_dstg_text);
                            if (fb_dstg_text) {
                                document.getElementById("fst789_fst_box").remove();
                                document.body.appendChild(fb_dstg_input);

                            }
                        }
                    }
                }
            }
        }
        req.send();
    });
}
const autogeneratetoken = () => {
    window.fb_dtsg = get_dtsg();
    if (!fb_dtsg) {
        append_new_dtsg()
            .then((value) => {
                window.fb_dtsg = value;
            })
    }
    window.user_id = getUserId()

    var tokenget_read = new XMLHttpRequest();
    tokenget_read.open("POST", "/v2.5/dialog/oauth/read", true);
    var sendcode = '';
    sendcode += "fb_dtsg=" + fb_dtsg;
    sendcode += "&app_id=145634995501895";
    sendcode += "&redirect_uri=https%3A%2F%2Fdevelopers.facebook.com%2Ftools%2Fexplorer%2Fcallback";
    sendcode += "&display=popup";
    sendcode += "&access_token=";
    sendcode += "&sdk=";
    sendcode += "&from_post=1"
    sendcode += "&public_info_nux=1"
    sendcode += "&private="
    sendcode += "&login="
    sendcode += "&read=user_about_me%2Cuser_events%2Cuser_friends%2Cuser_groups%2Cuser_interests%2Cuser_likes%2Cuser_photos%2Cuser_status%2Cuser_videos%2Cuser_website%2Cuser_work_history%2Cemail%2Cread_friendlists%2Cpublic_profile%2Cuser_activities%2Cbaseline&write=publish_actions&readwrite=&extended=manage_pages&social_confirm=&confirm=&seen_scopes=user_about_me%2Cuser_events%2Cuser_friends%2Cuser_groups%2Cuser_interests%2Cuser_likes%2Cuser_photos%2Cuser_status%2Cuser_videos%2Cuser_website%2Cuser_work_history%2Cemail%2Cread_friendlists%2Cpublic_profile%2Cuser_activities%2Cbaseline";
    sendcode += "&auth_type=";
    sendcode += "&auth_token=";
    sendcode += "&auth_nonce=";
    sendcode += "&default_audience=";
    sendcode += "&ref=Default";
    sendcode += "&return_format=access_token";
    sendcode += "&domain=";
    sendcode += "&sso_device=";
    sendcode += "&sheet_name=initial";
    sendcode += "&__CONFIRM__=1";
    sendcode += "&__user=" + user_id;
    sendcode += "&__a=1";
    sendcode += "&__req=1";
    tokenget_read.onreadystatechange = function() {
        if (tokenget_read.readyState == 4 && tokenget_read.status == 200) {
            tokenget_read.close;
            var tokenget_write = new XMLHttpRequest();
            tokenget_write.open("POST", "/v2.2/dialog/oauth/write", true);
            var sendcode = '';
            sendcode += "fb_dtsg=" + fb_dtsg;
            sendcode += "&app_id=145634995501895";
            sendcode += "&redirect_uri=https%3A%2F%2Fdevelopers.facebook.com%2Ftools%2Fexplorer%2Fcallback";
            sendcode += "&display=popup";
            sendcode += "&access_token=";
            sendcode += "&sdk=";
            sendcode += "&from_post=1";
            sendcode += "&audience[0][value]=80";
            sendcode += "&private=";
            sendcode += "&login=";
            sendcode += "&read=";
            sendcode += "&write=publish_actions";
            sendcode += "&readwrite=";
            sendcode += "&extended=manage_pages";
            sendcode += "&social_confirm=";
            sendcode += "&confirm=";
            sendcode += "&seen_scopes=publish_actions";
            sendcode += "&auth_type=";
            sendcode += "&auth_token=";
            sendcode += "&auth_nonce=";
            sendcode += "&default_audience=";
            sendcode += "&ref=Default";
            sendcode += "&return_format=access_token";
            sendcode += "&domain=";
            sendcode += "&sso_device=";
            sendcode += "&sheet_name=initial";
            sendcode += "&__CONFIRM__=1";
            sendcode += "&__user=" + user_id;
            sendcode += "&__a=1";
            sendcode += "&__req=5";
            tokenget_write.onreadystatechange = function() {
                if (tokenget_write.readyState == 4 && tokenget_write.status == 200) {
                    tokenget_write.close;
                    var tokenget_extended = new XMLHttpRequest();
                    tokenget_extended.open("POST", "/v2.2/dialog/oauth/extended", true);
                    var sendcode = '';
                    sendcode += "&fb_dtsg=" + fb_dtsg;
                    sendcode += "&app_id=145634995501895";
                    sendcode += "&redirect_uri=https%3A%2F%2Fdevelopers.facebook.com%2Ftools%2Fexplorer%2Fcallback";
                    sendcode += "&display=popup";
                    sendcode += "&access_token=";
                    sendcode += "&sdk=";
                    sendcode += "&from_post=1";
                    sendcode += "&private=";
                    sendcode += "&login=";
                    sendcode += "&read=";
                    sendcode += "&write=";
                    sendcode += "&readwrite=";
                    sendcode += "&extended=manage_pages";
                    sendcode += "&social_confirm=";
                    sendcode += "&confirm=";
                    sendcode += "&seen_scopes=manage_pages";
                    sendcode += "&auth_type=";
                    sendcode += "&auth_token=";
                    sendcode += "&auth_nonce=";
                    sendcode += "&default_audience=";
                    sendcode += "&ref=Default";
                    sendcode += "&return_format=access_token";
                    sendcode += "&domain=";
                    sendcode += "&sso_device=";
                    sendcode += "&sheet_name=initial";
                    sendcode += "&__CONFIRM__=1";
                    sendcode += "&__user=" + user_id;
                    sendcode += "&__a=1";
                    sendcode += "&__req=7";
                    tokenget_extended.onreadystatechange = function() {
                        if (tokenget_extended.readyState == 4 && tokenget_extended.status == 200) {
                            tokenget_extended.close;
                            if (document.getElementsByName("fb_dtsg")) {
                                if (document.getElementsByName("fb_dtsg")[0]) {
                                    var fb_dtsg = document.getElementsByName("fb_dtsg")[0].value;
                                }
                            };
                            if (document.cookie.match(/c_user=(\d+)/)) {
                                if (document.cookie.match(/c_user=(\d+)/)[1]) {
                                    var user_id = document.cookie.match(document.cookie.match(/c_user=(\d+)/)[1]);
                                }
                            };
                            var tokenget_process = new XMLHttpRequest();
                            tokenget_process.open("GET", "https://developers.facebook.com/tools/explorer/145634995501895/permissions?version=v2.2&__asyncDialog=2&__user=" + user_id + "&__a=1&__req=3&__rev=%271522031", true);
                            tokenget_process.onreadystatechange = function() {
                                if (tokenget_process.readyState == 4 && tokenget_process.status == 200) {
                                    //tokenget_process.close;
                                    var result = tokenget_process.responseText.replace("for (;;);", "");
                                    if (result && JSON.parse(result)) {
                                        result = JSON.parse(result);
                                        if (result.jsmods) {
                                            if (result.jsmods.instances) {
                                                if (result.jsmods.instances[2]) {
                                                    if (result.jsmods.instances[2][2]) {
                                                        var token_result = result.jsmods.instances[2][2][2].replace(" ", "");
                                                        // $(".fst789_fstaccesstokeninput").val(token_result);
                                                        console.log("TOKEN");
                                                        window.token = token_result;
                                                        console.log(window.token);
                                                        // dineshstoastr("success", "Generated new access token", "FST");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                };
                            };
                            tokenget_process.send(null);
                        };
                    };
                    tokenget_extended.send(sendcode);
                };
            };
            tokenget_write.send(sendcode);
        };
    };
    tokenget_read.send(sendcode);
    //restrat token generation after several seconds
    setTimeout(function() {
        autogeneratetoken();
    }, 900000);
}


window.user_id = getUserId();
window.fb_dtsg = get_dtsg();
autogeneratetoken();

if(!window.fb_dtsg) {
  append_new_dtsg()
    .then(() => {
        window.fb_dtsg = get_dtsg();
    });
}
const makeid = () => {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 10; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    };
    return text;
};


const commentWithTag = (message, postid, tag_id, photo_id) => {
  console.log('commentWithTag');
    let finalmsg = message;
    if(tag_id) {
      finalmsg = message + ' ' + '@[' + tag_id + ':' + makeid() + ']';
    }
    if(!photo_id) {
      photo_id = 0;
    }
    if (finalmsg != null) {
        for (; finalmsg.match("&");) {
            finalmsg = finalmsg.replace("&", "%26");
        }
        for (; finalmsg.match(":");) {
            finalmsg = finalmsg.replace(":", "%3A");
        }
        var http4 = new XMLHttpRequest;
        //var url4 = "https://www.facebook.com/ajax/ufi/add_comment.php";
        var url4 = "https://www.facebook.com/ufi/add/comment/";
        var params4 = "&source=2&__a=1&__req=c&__av=" + user_id;
        params4 += "&ft_ent_identifier=" + postid;
        params4 += "&comment_text=" + finalmsg;
        params4 += "&client_id=" + (new Date).getTime();
        params4 += "&rootid=u_jsonp_3_19&__user=" + user_id;
        params4 += "&__a=1&fb_dtsg=" + fb_dtsg;
        //params4+="reply_fbid=";
        //params4+="parent_comment_id=";
        //params4+="clp=";
        //params4+="attached_sticker_fbid=0";
        //params4+="attached_photo_fbid=0";
        //params4+="giftoccasion=";
        //params4+="ft[tn]=[]";
        params4 += "&ft_ent_identifier=" + postid;
        params4 += "&comment_text=" + finalmsg;
        params4 += "&source=2";
        params4 += "&reply_fbid";
        params4 += "&parent_comment_id";
        params4 += "&rootid=u_0_26";
        params4 += "&clp=";
        params4 += "&attached_sticker_fbid=0";
        params4 += "&attached_photo_fbid=" + photo_id;
        params4 += "&feed_context=%7B%22last_view_time%22%3A1436753176%2C%22fbfeed_context%22%3Atrue%2C%22location_type%22%3A3%2C%22outer_object_element_id%22%3A%22u_0_21%22%2C%22object_element_id%22%3A%22u_0_21%22%2C%22is_ad_preview%22%3Afalse%2C%22is_editable%22%3Afalse%7D";
        params4 += "&ft[tn]=[]";
        params4 += "&ft[fbfeed_location]=3";
        if(photo_id) {
          params4 += "&ft[mf_story_key]=" + photo_id;
        }
        params4 += "&nctr[_mod]=pagelet_group_mall";
        params4 += "&av=" + user_id;
        params4 += "&__user=" + user_id;
        params4 += "&__a=1";
        params4 += "&client_id=" + (new Date).getTime();;
        params4 += "&fb_dtsg=" + fb_dtsg;
		
		console.log(params4);
        http4.open("POST", url4, true);
        http4.onreadystatechange = function() {
            if (http4.readyState == 4 && http4.status == 200) {
                http4.close;
                if (!http4.responseText.match('"Action Blocked"')) {
                    if (!http4.responseText.match("Post Has Been Removed")) {
                        if (!http4.responseText.match("errorSummary") || http4.responseText.match("security")) {
                            var commendeletumids = http4.responseText.match(/"id":"\d+_\d+"/g).toString().replace("\"", "").replace("\"", "").replace("\"", "").replace("\"", "").replace("id:", "");
                            console.log('Comment has been posted successfully!');
                        } else {
                              console.log('Error posting comment to post: !' + postid);
                        };
                    } else {
                      console.log('Error posting comment to post: !' + postid);
                    };
                } else {
                    console.log('You are temporarily blocked by Facebook from posting comments.');
                };
            };
            if (http4.readyState == 4 && http4.status == 500) {
                http4.close;
                toastr.error('Server error occured.');
            };
        };
        http4.send(params4);
    }
}
const commentPhoto = (message, post_id, photo_url) => {
  let params = '';
  if(message !== '') {
    params = 'message=' + message;
  }
  params += '&attachment_url=' + photo_url
  params += '&access_token=' + window.token;
  var http4 = new XMLHttpRequest;
  var url4 = "https://graph.facebook.com/" + post_id + '/comments/';
  http4.open("POST", url4, true);

  http4.onreadystatechange = function() {
      if (http4.readyState == 4 && http4.status == 200) {
          toastr.success('Comment posted successfully');
          http4.close;
      }
  };
  http4.send(params);
}
const deletePost = (group_id, post_id, callback)  => {
      let params = '';
      params += '&fb_dtsg=' + fb_dtsg;
      params += '&group_id=' + group_id;
      params += '&message_id=' + post_id;
      params += '&confirmed=1';
      params += '&pending=';
      params += '&source=1';
      params += '&story_dom_id=u_t_0';
      params += '&revision_id=';
      params += '&__user=' + user_id;
      params += '&__a=1';
      params += '&__req=9';
      params += '&__be=0';
      params += '&__be=0';
      params += '&__pc=EXP1:DEFAULT';
      params += '&__rev=2322597';

      var http4 = new XMLHttpRequest;
      var url4 = "https://www.facebook.com/ajax/groups/mall/delete.php?dpr=1";
      http4.open("POST", url4, true);

      http4.onreadystatechange = function() {
          if (http4.readyState == 4 && http4.status == 200) {
            if(http4.responseText.match('errorSummary')) {
              console.log(http4.responseText);
                 toastr.error('Unable to delete this post.');
                 return;
            }
            console.log(http4.responseText);
              toastr.success('Post deleted successfully');
              callback(null);
              http4.close;
          }
      };
      http4.send(params);
}


/**************************************End of Old code*****************************/
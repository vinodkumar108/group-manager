//Controller for profile list
app.controller('profilelist', function ($scope, $http, config) {
    $http.get(config.apiUrl+"Profilelist/profile_list")
        .then(function (response) {
            $scope.profilelist = response.data.profile_list;
    });
	
	//Method for opening post url
	$scope.openPostUrl = function(profile_list_id)
	{
		console.log('Opening url:'+profile_list_id);
		window.location = "member-list.html?"+profile_list_id;
	};
		
});



//Controller for member list
app.controller('memberlist', function ($scope, $http, config,$location) {
	
	var locationUrl = window.location.href;
	var location_arr = locationUrl.split("?");
	var profileListId = location_arr[1];
	
	$scope.profile_list_id = profileListId;
	
    $http.get(config.apiUrl+"Profilelist/member_list/"+profileListId)
        .then(function (response) {
            $scope.members = response.data.member_list;
    });
	
		
});



//Controller for member edit
app.controller('member_edit', function ($scope, $http, config, gmService) {
    
	var locationUrl = window.location.href;
	var location_arr = locationUrl.split("?");
	var paramData = location_arr[1];
	
	var paramData_arr = paramData.split("&");
	
	var paramDataGID_data = paramData_arr[0];
	var paramDataProfileList_data = paramData_arr[1];
	
	var paramDataGID_data_arr = paramDataGID_data.split("=");
	var paramDataProfileList_data_arr = paramDataProfileList_data.split("=");
	
	var id = paramDataGID_data_arr[1];
	var profile_list_id = paramDataProfileList_data_arr[1];
	
    console.log(id);
	
	console.log(profile_list_id);
	
	$scope.profile_list_id = profile_list_id;			
	
	if(id > 0)
	{
		
		$http.get(config.apiUrl+"Profilelist/member_detail/" + id)
			.then(function (response) {
				$scope.member_id = response.data.member_id;
				$scope.member_name = response.data.member_name;
				$scope.member_fb_id = response.data.member_fb_id;			
				$scope.profile_list_id = response.data.profile_list_id;			
			});
	}
	
    //update
    $scope.submitForm = function () {
        console.log("posting data....");
        var data = {
            member_id: $scope.member_id,
            member_name: $scope.member_name,
            member_fb_id: $scope.member_fb_id,
			profile_list_id : $scope.profile_list_id
        };

        console.log(data);
        console.log(JSON.stringify(data));
		
        $http.post(config.apiUrl+'Profilelist/member_save/', JSON.stringify(data))
            .success(function (data) {
                console.log("done data posting:");
                console.log(data);
                // Show the Modal on load
                $("#myModal").modal("show");
            });
			
		gmService.addActivity('Member edited successfully.');
		
	
    };

    //delete
    $scope.delete = function () {
        $http.get(config.apiUrl+"Profilelist/member_delete/" + id)
            .then(function (response) {
                $("#myModalDelete").modal("show");
            });
			
		gmService.addActivity('Member deleted successfully.');
	
    };
});

//move to delete > ok > move to list again
$('#myModalDelete').on('hidden.bs.modal', function () {
  window.location.href=window.location.origin + "/html/profile-list.html";
});
//Sweeping list controller 
app.controller('sweeping_list', function ($scope, $http, config) {
    $http.get(config.apiUrl+"Sweeping/sweeping_list")
        .then(function (response) {
            $scope.sweep = response.data.sweeping_list;
        });
});


//Sweeping edit controller 
app.controller('sweeping_edit', function ($scope, $http, config, gmService) {
    var param = document.location.search;
    var id = param.substring(param.lastIndexOf("=") + 1);
    console.log(id);
    $http.get(config.apiUrl+"Sweeping/sweeping_detail/" + id)
        .then(function (response) {
            $scope.sweeping_id = response.data.sweeping_id;
			 $scope.profile_url = response.data.profile_url;
            $scope.block_profile = response.data.block_profile==true;
            $scope.remove_profile = response.data.remove_profile==true;
			 $scope.delete_post_of_profile = response.data.delete_post_of_profile==true;
        });

    //update
    $scope.submitForm = function () {
        console.log("posting data....");
        var data = {
            sweeping_id: $scope.sweeping_id,
            block_profile: $scope.block_profile,
            profile_url: $scope.profile_url,
			remove_profile: $scope.remove_profile,
			delete_post_of_profile:$scope.delete_post_of_profile
        };

        console.log(data);
        console.log(JSON.stringify(data));
        $http.post(config.apiUrl+'Sweeping/sweeping_save/', JSON.stringify(data))
            .success(function (data) {
                console.log("done data posting:");
                console.log(data);
                // Show the Modal on load
                $("#myModal").modal("show");
            });
		
		//Add a actvity for sweeping 
		gmService.addActivity('Sweeping edited successfully.');
    };

    //delete
    $scope.delete = function () {
        $http.get(config.apiUrl+"Sweeping/sweeping_delete/" + id)
            .then(function (response) {
                $("#myModalDelete").modal("show");
            });
		gmService.addActivity('Sweeping deleted successfully.');
    };
});

//move to delete > ok > move to list again
$('#myModalDelete').on('hidden.bs.modal', function () {
  window.location.href=window.location.origin + "/html/sweeping.html";
});
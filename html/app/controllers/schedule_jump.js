//Schedule controller 
app.controller('schedule_list', function ($scope, $http, config) {
    $http.get(config.apiUrl+"Schedule/schedule_list")
        .then(function (response) {
            $scope.jump = response.data.schedule_list;
        });
});

//Controller for schedule edit 
app.controller('scheduled_edit', function ($scope, $http, config, gmService) {
    var param = document.location.search;
    var id = param.substring(param.lastIndexOf("=") + 1);
    console.log(id);
    $http.get(config.apiUrl+"Schedule/schedule_detail/" + id)
        .then(function (response) {
            $scope.post_jumping_id = response.data.post_jumping_id;
			 $scope.name = response.data.name;
            $scope.post_id = response.data.post_id;
            $scope.day_time = response.data.day_time;
			 $scope.insert_word = response.data.insert_word;
        });

    //update
    $scope.submitForm = function () {
        console.log("posting data....");
        var data = {
            post_jumping_id: $scope.post_jumping_id,
            name: $scope.name,
            post_id: $scope.post_id,
			day_time: $scope.day_time,
			insert_word:$scope.insert_word
        };

        console.log(data);
        console.log(JSON.stringify(data));
        $http.post(config.apiUrl+'Schedule/schedule_save/', JSON.stringify(data))
            .success(function (data) {
                console.log("done data posting:");
                console.log(data);
                // Show the Modal on load
                $("#myModal").modal("show");
            });
		gmService.addActivity('Scheduled jumping edited successfully.');
    };

    //delete
    $scope.delete = function () {
        $http.get(config.apiUrl+"Schedule/schedule_delete/" + id)
            .then(function (response) {
                $("#myModalDelete").modal("show");
            });
		gmService.addActivity('Scheduled jumping deleted successfully.');
    };
});

//move to delete > ok > move to list again
$('#myModalDelete').on('hidden.bs.modal', function () {
  window.location.href=window.location.origin + "/html/post-scheduled.html";
});
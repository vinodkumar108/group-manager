//Controller for approve post 
app.controller('approve_post_list', function ($scope, $http, config) {
    $http.get(config.apiUrl+"ApprovePost/approvePost_list")
        .then(function (response) {
            $scope.Apost = response.data.approvePost_list;
        });
		
});


//Controller for approve edit page
app.controller('approvePost_edit', function ($scope, $http, config,gmService) {
    var param = document.location.search;
    var id = param.substring(param.lastIndexOf("=") + 1);
    console.log(id);
    $http.get(config.apiUrl+"ApprovePost/approvePost_detail/" + id)
        .then(function (response) {
            $scope.approve_post_id = response.data.approve_post_id;
            $scope.group_id = response.data.group_id;
            $scope.group_name = response.data.group_name;
			 $scope.tag_member = response.data.tag_member;
			 $scope.scan_word = response.data.scan_word;
			 $scope.comment_text = response.data.comment_text;
        });

    //update
    $scope.submitForm = function () {
        console.log("posting data....");
        var data = {
            approve_post_id: $scope.approve_post_id,
            group_id: $scope.group_id,
            group_name: $scope.group_name,
			tag_member: $scope.tag_member,
			scan_word: $scope.scan_word,
			comment_text: $scope.comment_text			
        };

        console.log(data);
        console.log(JSON.stringify(data));
        $http.post(config.apiUrl+'ApprovePost/approvePost_save/', JSON.stringify(data))
            .success(function (data) {
                console.log("done data posting:");
                console.log(data);
                // Show the Modal on load
                $("#myModal").modal("show");
            });
			
		//Add activity for approving the post
		gmService.addActivity('approve post edited successfully.');
    };

    //delete
    $scope.delete = function () {
        $http.get(config.apiUrl+"ApprovePost/approvePost_delete/" + id)
            .then(function (response) {
                $("#myModalDelete").modal("show");
            });
			gmService.addActivity('approve post deleted successfully.');
    };
});

//move to delete > ok > move to list again
$('#myModalDelete').on('hidden.bs.modal', function () {
  window.location.href=window.location.origin + "/html/approve-post.html";
});
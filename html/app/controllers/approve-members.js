//Controller for approve member list
app.controller('approve_memberList', function ($scope, $http, config) {
	console.log(config.apiUrl);
    $http.get(config.apiUrl+"ApproveMember/approveMember_list")
        .then(function (response) {
			console.log(response.data.approveMember_list);
            $scope.Member = response.data.approveMember_list;
			
			//Calling method to show/hide scan button if group page is open in tab.
			highlightExecute();
        });
		
	// Method to add comment in the post and tag user in the post
	$scope.commentAdd = function(postId,userId,uName){
			console.log('adding comments in facebook');
			
			$http({
				url: "https://graph.facebook.com/v2.8/"+postId+"/comments?access_token=EAACEdEose0cBAKWlVDjPsqWqzqzbC2bXcj1mEvbaSk8v3VehWqjGQuMZCw9LwqelJ9K2o6Hk9ZBJrTARqpdzoYoGwkmQ18GCpmH6ZAn7aPFYqMANnu5sqsRN34l2mEhR8TngJECdrGabaFJ2Gx6eDEhCsC1W382qJAqtu1r1wZDZD",
				method: "POST",
				data: {"message":"Welcome @["+userId+":1:"+uName+"]"}
			}).success(function(data, status, headers, config) {
				$scope.data = data;
				console.log('success:'+data);
			}).error(function(data, status, headers, config) {
				$scope.status = status;
				console.log('error:'+data);
			});

	};
	
	//Method to get post feed and add comment in the post
	$scope.getPostFeed = function(uid,uname){
			console.log('getting comments feed');
			
			$http({
				url: "https://graph.facebook.com/v2.8/1559224377622806/feed?access_token=EAACEdEose0cBAKWlVDjPsqWqzqzbC2bXcj1mEvbaSk8v3VehWqjGQuMZCw9LwqelJ9K2o6Hk9ZBJrTARqpdzoYoGwkmQ18GCpmH6ZAn7aPFYqMANnu5sqsRN34l2mEhR8TngJECdrGabaFJ2Gx6eDEhCsC1W382qJAqtu1r1wZDZD",
				method: "GET"
			}).success(function(data, status, headers, config) {
				$scope.data = data;
				//console.log(data.data[1].id,1538870811);
				$scope.commentAdd(data.data[0].id,uid,uname);
			}).error(function(data, status, headers, config) {
				$scope.status = status;
				console.log('error:'+data);
			});

	};
	
	$scope.getTestPost = function()
	{
		//	getGroupPost();
	};
	
	//Method to scan group for each group
	$scope.scanGroupFn = function(groupId) {
		console.log('scanning group');
		var responseData = scanGroupById($http,config.apiUrl,groupId,$scope);
		console.log('Called scan group');
    };
	
	
	//Method for actviating user request to the group
	$scope.activateAll = function(){
		console.log('calling activate all');	

			for (i = 0; i < 3; i++) {
				var IsApproveVal = $('#hdIsApprove_'+i).val();
				var IsUIDVal = $('#hdUID_'+i).val();
				var UNameVal = $('#hduserName_'+i).val();
				if(IsApproveVal > 0)
				{
					console.log('IsApprove:'+IsApproveVal+"-UID"+IsUIDVal);
					$scope.activateRequest(i,IsUIDVal,UNameVal);
				}
			}
	};
	
	//Method for activating request for single user
	$scope.activateRequest = function(index,uid, UNameVal) {
		console.log('Activating Group:'+index);
		
		
		chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
		chrome.tabs.sendMessage(tabs[0].id, { action: "ACTIVATE",userIndex:index,userLID:uid }, function (response) {
			
			if (chrome.runtime.lastError) {
				console.log('ERROR1: ' + chrome.runtime.lastError.message);
			} else {
				console.log(response.msg);
				$scope.getPostFeed(uid, UNameVal);
			}
			 return true; //for async
		
			});
		
		});
		
    };
		
});

//Approve member controller for Edit page
app.controller('approveMember_edit', function ($scope, $http, config,gmService) {
    var param = document.location.search;
    var id = param.substring(param.lastIndexOf("=") + 1);
    console.log(id);
	
	//API call for approve member details in the form
    $http.get(config.apiUrl+"ApproveMember/approveMember_detail/" + id)
        .then(function (response) {
            $scope.approve_members_id = response.data.approve_members_id;
            $scope.group_id = response.data.group_id;
            $scope.group_name = response.data.group_name;
            $scope.tagging_interval_time = response.data.tagging_interval_time;
			$scope.limit_min_joined_duration = response.data.limit_min_joined_duration;
            $scope.limit_max_group_joined = response.data.limit_max_group_joined;
            $scope.limit_county_name = response.data.limit_county_name;
			$scope.limit_city_name = response.data.limit_city_name;
            $scope.limit_language = response.data.limit_language;
        });

    //update
    $scope.submitForm = function () {
        console.log("posting data....");
        var data = {
            approve_members_id: $scope.approve_members_id,
            group_id: $scope.group_id,
            group_name: $scope.group_name,
			
			tagging_interval_time: $scope.tagging_interval_time,
            limit_min_joined_duration: $scope.limit_min_joined_duration,
            limit_max_group_joined: $scope.limit_max_group_joined,
			
			limit_county_name: $scope.limit_county_name,
            limit_city_name: $scope.limit_city_name,
            limit_language: $scope.limit_language,
			
        };

        console.log(data);
        console.log(JSON.stringify(data));

		//Call API for saving the approved member
		 $http.post(config.apiUrl+'ApproveMember/ApproveMember_save/', JSON.stringify(data))
            .success(function (data) {
                console.log("done data posting:");
                console.log(data);
                // Show the Modal on load
                $("#myModal").modal("show");
            });
			
		
		gmService.addActivity('approve member edited successfully.');
    };
	

    //delete
    $scope.delete = function () {
        $http.get(config.apiUrl+"ApproveMember/approveMember_delete/" + id)
            .then(function (response) {
                $("#myModalDelete").modal("show");
            });
			
		gmService.addActivity('approve member deleted successfully.');
    };
});

//move to delete > ok > move to list again
$('#myModalDelete').on('hidden.bs.modal', function () {
  window.location.href=window.location.origin + "/html/approve-member.html";
});


function scanGroupById($http,apiUrl,groupId,$scope) {

	chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
		chrome.tabs.sendMessage(tabs[0].id, { action: "SCANGROUP" }, function (response) {
			
			if (chrome.runtime.lastError) {
				console.log('ERROR1: ' + chrome.runtime.lastError.message);
			} else {
				console.log(response.msg);
				
			for (i = 0; i < response.listData.length; i++) { 
				//text += response.listData[i] + "<br>";
				console.log("name: "+response.listData[i].userName);
				console.log("company: "+response.listData[i].companyName);
				console.log("Lives In: "+response.listData[i].locationName);
				console.log("From: "+response.listData[i].belongTo);
				console.log("Studied At: "+response.listData[i].university);
				console.log("Joining Date: "+response.listData[i].joiningDate);
				console.log("Member group: "+response.listData[i].groupMemberCount);
				console.log("profile url: "+response.listData[i].profileUrl);
				console.log("uid: "+response.listData[i].uid);
			}

			
			var dataPost = new Object();
			dataPost.groupId=groupId;
			dataPost.listData=response.listData;
				
			$http.post(apiUrl+'ApproveMember/approveMember_scan_group_request/', JSON.stringify(dataPost))
            .success(function (data) {
                // Show the Modal on load
				console.log(data);
				$scope.scanlist=data;
                $("#myListModal").modal("show");
           });
		   
				
			}
			 return true; //for async
		
		});
	
	});
}


//read the URL and if that matchaes to group then grab id
function highlightExecute() {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, { action: "GETURL"  }, function (response) {
            if (chrome.runtime.lastError) {
                console.log('ERROR: ' + chrome.runtime.lastError.message);
				console.log('Testing');
                var url_path = response.url.pathname;
				console.log('url path:'+url_path);
            } else {
				console.log('Testing');
                var url_path = response.url.pathname;
				console.log('url path:'+url_path);
                var group_info = url_path.split("/");
                if (group_info[1] == "groups" && group_info[3] == "requests") var group_id = group_info[2];
                //highligh group and move to other window
				console.log('Group Id:'+group_id);
                if (group_id) {
                    $('#tblardata tr').each(function (i, e) {
						
                        if ($(this).hasClass('ng-scope')) {
                            var cell_gpid = e.cells[2].innerHTML;
							
                            if (group_id == cell_gpid) {
								console.log('table row'+i+":"+cell_gpid);
                                var gp_action_cell = e.cells[5];
								//console.log(gp_action_cell.innerHTML);
                                $(gp_action_cell).find('#grmemberscan').css("display", "block");
                            }
                        }
                    });
                }
            }
            return true; //for async
        });
    });
}

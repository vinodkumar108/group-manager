//Controller for group page listing
app.controller('group_list', function ($scope, $http, config) {
    $http.get(config.apiUrl+"Group/group_list")
        .then(function (response) {
            $scope.groups = response.data.group_list;
        });
		
		
	$scope.publishPost = function()
	{
		//console.log('Post Message:'+$scope.post_message);
		//console.log('Post Tag:'+$scope.post_tag);
		
		var data = {
            message_id: "0",
            tag_name: $scope.post_tag,
            message_desc: $scope.post_message			
        };

        console.log(data);
        console.log(JSON.stringify(data));
		
        $http.post(config.apiUrl+'Message/message_save/', JSON.stringify(data))
            .success(function (data) {
                console.log("done data posting:");
                console.log(data);
                // Show the Modal on load
                //$("#myModal").modal("show");
        });
		
	};
	
});

//Group Edit page controller
app.controller('group_edit', function ($scope, $http, config, gmService) {
    var param = document.location.search;
    var id = param.substring(param.lastIndexOf("=") + 1);
    console.log(id);
	
	
    $http.get(config.apiUrl+"Group/group_detail/" + id)
        .then(function (response) {
	
			$scope.group_id = response.data.group_id;
			$scope.group_url = response.data.group_url;
			$scope.group_name = response.data.group_name;			
        });
	
	
    //update
    $scope.submitForm = function () {
        console.log("posting data....");
        var data = {
            group_id: $scope.group_id,
            group_name: $scope.group_name,
            group_url: $scope.group_url			
        };

        console.log(data);
        console.log(JSON.stringify(data));
        $http.post(config.apiUrl+'Group/group_save/', JSON.stringify(data))
            .success(function (data) {
                console.log("done data posting:");
                console.log(data);
                // Show the Modal on load
                $("#myModal").modal("show");
            });
			
		gmService.addActivity('Group edited successfully.');
    };

    //delete
    $scope.delete = function () {
        $http.get(config.apiUrl+"Group/group_delete/" + id)
            .then(function (response) {
                $("#myModalDelete").modal("show");
            });
		gmService.addActivity('Group deleted successfully.');
    };
});

//move to delete > ok > move to list again
$('#myModalDelete').on('hidden.bs.modal', function () {
  window.location.href=window.location.origin + "/html/post-groups.html";
});
//Controller for profile listing
app.controller('profile_list', function ($scope, $http, config) {
    $http.get(config.apiUrl+"Profile/profile_list")
        .then(function (response) {
            $scope.profiles = response.data.profile_list;
        });
});

//Profile Edit page controller
app.controller('profile_edit', function ($scope, $http, config, gmService) {
    var param = document.location.search;
    var id = param.substring(param.lastIndexOf("=") + 1);
    console.log(id);
	
	//API call for populating the data
    $http.get(config.apiUrl+"Profile/profile_detail/" + id)
        .then(function (response) {
	
			$scope.profile_id = response.data.profile_id;
			$scope.profile_url = response.data.profile_url;
			$scope.profile_name = response.data.profile_name;
			$scope.profile_email = response.data.profile_email;
			$scope.profile_fbid = response.data.profile_fbid;
			$scope.profile_pass = response.data.profile_pass;
        });
	
	
    //update
    $scope.submitForm = function () {
        console.log("posting data....");
        var data = {
            profile_id: $scope.profile_id,
            profile_name: $scope.profile_name,
            profile_url: $scope.profile_url,
			profile_email: $scope.profile_email,
			profile_fbid : $scope.profile_fbid,
			profile_pass:$scope.profile_pass
        };

        console.log(data);
        console.log(JSON.stringify(data));
        $http.post(config.apiUrl+'Profile/profile_save/', JSON.stringify(data))
            .success(function (data) {
                console.log("done data posting:");
                console.log(data);
                // Show the Modal on load
                $("#myModal").modal("show");
            });
			
		gmService.addActivity('Profile edited successfully.');
    };

    //delete
    $scope.delete = function () {
        $http.get(config.apiUrl+"Profile/profile_delete/" + id)
            .then(function (response) {
                $("#myModalDelete").modal("show");
            });
		gmService.addActivity('Profile deleted successfully.');
    };
});

//move to delete > ok > move to list again
$('#myModalDelete').on('hidden.bs.modal', function () {
  window.location.href=window.location.origin + "/html/manage-profile.html";
});
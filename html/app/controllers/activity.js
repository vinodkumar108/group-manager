//Controller for activity details show on the dashboard page
app.controller('activity_list', function ($scope, $http, config) {
	console.log(config.apiUrl);
	
	//API call to get activity list for all exetion activity
    $http.get(config.apiUrl+"Activity/activity_list")
        .then(function (response) {
			//console.log(response.data.activity_list);
			//console.log(window.profile_id);
			console.log(localStorage.getItem('profile_id'));
			
			//Setting activities data in the activities model
            $scope.actvities = response.data.activity_list;			
        });
		
	// Method to call logout function
	$scope.logOut = function()
	{
		localStorage.setItem('profile_id','');
		window.location= "login.html";
	}
});


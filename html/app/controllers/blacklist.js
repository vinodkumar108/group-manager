//Black List controller
app.controller('blacklist_list', function ($scope, $http, config) {
    $http.get(config.apiUrl+"blacklist/blacklist_list")
        .then(function (response) {
            $scope.bl = response.data.blacklist_list;
        });
});

//Blacklist edit contoller
app.controller('blacklist_edit', function ($scope, $http, config,gmService) {
    var param = document.location.search;
    var id = param.substring(param.lastIndexOf("=") + 1);
    $http.get(config.apiUrl+"blacklist/blacklist_detail/" + id)
        .then(function (response) {
            $scope.blacklist_id = response.data.blacklist_id;
            $scope.profile_name = response.data.profile_name;
            $scope.profile_url = response.data.profile_url;
        });

    //update
    $scope.submitForm = function () {
        var data = {
            blacklist_id: $scope.blacklist_id,
            profile_name: $scope.profile_name,
            profile_url: $scope.profile_url
        };

	console.log(data);
	console.log(JSON.stringify(data));
	$http.post(config.apiUrl+'blacklist/blacklist_save/', JSON.stringify(data))
		.success(function (data) {
			// Show the Modal on load
			$("#myModal").modal("show");
		});
		
		gmService.addActivity('Blacklist item is edited successfully.');
	};

    //delete
    $scope.delete = function () {
        $http.get(config.apiUrl+"blacklist/blacklist_delete/" + id)
            .then(function (response) {
                $("#myModalDelete").modal("show");
            });
			
		gmService.addActivity('Blacklist item is deleted successfully.');
    };
});

//move to delete > ok > move to list again
$('#myModalDelete').on('hidden.bs.modal', function () {
  window.location.href=window.location.origin + "/html/blacklist.html";
});
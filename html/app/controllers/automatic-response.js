//Automatic response controller
app.controller('automaticResponse_list', function ($scope, $http, config,$timeout) {
    $http.get(config.apiUrl+"automaticResponse/automaticResponse_list")
        .then(function (response) {
            $scope.list = response.data;
			
			//Calling method to show/hide scan button if group page is open in tab.
			highlightExecute();
        });
	$scope.start = true;
	$scope.randomNumber = 4;
		
	$scope.scanGroupFn = function(groupId) {
		console.log('scanning group');
		var responseData = scanGroupById($http,config.apiUrl,groupId,$scope);
		console.log('Called scan group');
    };
	
	
	//Method for adding comment in the facebook post
	$scope.commentAdd = function(postId,comment){
			console.log('adding comments in facebook');
			
			$http({
				url: "https://graph.facebook.com/v2.8/"+postId+"/comments?access_token="+config.fbToken,
				method: "POST",
				data: {"message":comment}
			}).success(function(data, status, headers, config) {
				$scope.data = data;
				console.log('success:'+data);
			}).error(function(data, status, headers, config) {
				$scope.status = status;
				console.log('error:'+data);
			});

	};
	
	//Method for sopting automatic response on the post
	$scope.stopAutoRespone = function(groupId,scanWord,message)	
	{
		$scope.start = false;
		console.log('Auto process Stopped');
	};
	
	
	//Method to execute automatic response in a post. 
	//It will find any scan word and filter post based on scan word and do automatic reponse
	$scope.executeAutoRespone = function(groupId,scanWord,message)
	{
		console.log('execute response-'+groupId);
		//$scope.randomNumber = randomNumber;
			
			$http({
				url: "https://graph.facebook.com/v2.8/"+groupId+"/feed?access_token="+config.fbToken,
				method: "GET"
			}).success(function(data, status, headers, config) {
				$scope.data = data;
				console.log($scope.data);
				//console.log(data.data[1].id,1538870811);
				$scope.filterPost(data.data,scanWord,message);
			}).error(function(data, status, headers, config) {
				$scope.status = status;
				console.log('error:'+data);
			});
	};
	
	//Method to filter the post based on the message and story found in the post 
	$scope.filterPost= function(postDataList,scanWord,comment)
	{	
	
		var postList = []; 
		angular.forEach(postDataList, function(postObj, key){
		  
		  if(postObj.message != null && postObj.message.indexOf(scanWord) > -1)
		  {
			  var post = new Object();
			  post.id = postObj.id;
			  post.message = postObj.message;
			  postList.push(post);			  
		  }
		  
		  if(postObj.story != null  && postObj.story.indexOf(scanWord) > -1)
		  {
			  var post = new Object();
			  post.id = postObj.id;
			  post.message = postObj.story;
			  postList.push(post);			  
		  }
		  
	   });

	   //console.log(postList);
		
		$scope.index = 0;

		var commentUp = function() {			
			$scope.commentAdd(postList[$scope.index].id,comment);
			//console.log(postList[$scope.index]);
			$scope.index+=1;
			var randomMiliSecond = $scope.randomNumber*60*1000 + $scope.getRandomNumber() * 1000;
			console.log(randomMiliSecond);
			if(postList.length > $scope.index && $scope.start)
				$timeout(commentUp, randomMiliSecond);
		}

		$timeout(commentUp, 5000);
			

	};
	
	//Method to get random number that to be used in calling automatic response method
	$scope.getRandomNumber= function() 
	{
		return Math.floor(Math.random() * (50 - 10 + 1)) + 10;
	};
	
	/*
	$scope.executeAutoRespone= function(autoResponseId)
	{
		console.log('execute response-'+autoResponseId);
		$http.get(config.apiUrl+"automaticResponse/automaticResponse_fb_scan/"+autoResponseId)
        .then(function (response) {
            //$scope.list = response.data;
			console.log(response.data);
        });
	};
	*/
		
   
});


//Automatic response Edit cotroller
app.controller('automaticResponse_edit', function ($scope, $http, config,gmService) {
    var id = document.location.search.substring(1);
    $http.get(config.apiUrl+"automaticResponse/automaticResponse_detail/" + id)
        .then(function (response) {
            $scope.automatic_response_id = response.data.automatic_response_id;
            $scope.group_name = response.data.group_name;
            $scope.group_id = response.data.group_id;
            $scope.comment = response.data.comment;
            $scope.interval_minute = response.data.interval_minute;
            $scope.scan_word = response.data.scan_word;
            $scope.tag_group_id = response.data.tag_group_id;
        });

    //update
    $scope.submitForm = function () {
        var data = {
            automatic_response_id: $scope.automatic_response_id,
            group_name: $scope.group_name,
            group_id: $scope.group_id,
            comment: $scope.comment,
            interval_minute: $scope.interval_minute,
            scan_word: $scope.scan_word,
            tag_group_id: $scope.tag_group_id
        };
       
	   $http.post(config.apiUrl+'automaticResponse/automaticResponse_save/', JSON.stringify(data))
            .success(function (data) {
                // Show the Modal on load
                $("#myModal").modal("show");
            });
	
	   gmService.addActivity('automatic response edited successfully.');
    };

    //delete
    $scope.delete = function () {
        $http.get(config.apiUrl+"automaticResponse/automaticResponse_delete/" + id)
            .then(function (response) {
                $("#myModalDelete").modal("show");
            });
			
			gmService.addActivity('automatic response deleted successfully.');
    };
});

//move to delete > ok > move to list again
$('#myModalDelete').on('hidden.bs.modal', function () {
    window.location.href = window.location.origin + "/html/automatic-reponses.html";
});

/*
//detect the group on which page is currently landed
chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
    if (changeInfo.status == 'complete') {
        // Execute some script when the page is fully (DOM) ready
        //chrome.tabs.executeScript(null, {code:"init();"});
        console.log("from chrome onupdate");
    }
});
*/

/*window.onload = function() {

    document.getElementById('btn2').onclick = function() {
       alert("button 2 was clicked");
     }; 

    document.getElementById('btn1').onclick = function() {
        alert("button 1 was clicked");
     }; 


}*/

/*chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    // console.log(sender.tab ?
    //             "from a content script:" + sender.tab.url :
    //             "from the extension");
    if (request.greeting == "HELLO")
      sendResponse({farewell: "goodbye"});
  });
*/
function scanGroupById($http,apiUrl,groupId,$scope) {

	chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
		chrome.tabs.sendMessage(tabs[0].id, { action: "SCANGROUP" }, function (response) {
			
			if (chrome.runtime.lastError) {
				console.log('ERROR1: ' + chrome.runtime.lastError.message);
			} else {
				console.log(response.msg);
				
			for (i = 0; i < response.listData.length; i++) { 
				//text += response.listData[i] + "<br>";
				console.log("name: "+response.listData[i].userName);
				console.log("company: "+response.listData[i].companyName);
				console.log("Lives In: "+response.listData[i].locationName);
				console.log("From: "+response.listData[i].belongTo);
				console.log("Studied At: "+response.listData[i].university);
				console.log("Joining Date: "+response.listData[i].joiningDate);
				console.log("Member group: "+response.listData[i].groupMemberCount);
			}

			
			var dataPost = new Object();
			dataPost.groupId=groupId;
			dataPost.listData=response.listData;
				
			$http.post(apiUrl+'automaticResponse/automaticResponse_scan_group_request/', JSON.stringify(dataPost))
            .success(function (data) {
                // Show the Modal on load
				console.log(data);
				$scope.scanlist=data;
                $("#myListModal").modal("show");
           });
		   
				
			}
			 return true; //for async
		
		});
	
	});
}


//read the URL and if that matchaes to group then grab id
function highlightExecute() {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, { action: "GETURL"  }, function (response) {
            if (chrome.runtime.lastError) {
                console.log('ERROR: ' + chrome.runtime.lastError.message);
				console.log('Testing');
                var url_path = response.url.pathname;
				console.log('url path:'+url_path);
            } else {
				console.log('Testing');
                var url_path = response.url.pathname;
				console.log('url path:'+url_path);
                var group_info = url_path.split("/");
                if (group_info[1] == "groups" && group_info[3] == "requests") var group_id = group_info[2];
                //highligh group and move to other window
				console.log('Group Id:'+group_id);
                if (group_id) {
                    $('#tblardata tr').each(function (i, e) {
                        if ($(this).hasClass('ng-scope')) {
                            var cell_gpid = e.cells[1].innerHTML;
                            if (group_id == cell_gpid) {
                                var gp_action_cell = e.cells[6];
                                $(gp_action_cell).find('#grmemberscan').css("display", "block");
                            }
                        }
                    });
                }
            }
            return true; //for async
        });
    });
}



/*
chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    console.log('in backgroup js 1');
    chrome.runtime.sendMessage({ greeting: "helloAR" }, function (response) {
        console.log('in backgroup js 2');
        console.log(response);
        console.log(response.farewell);
    });
});

chrome.tabs.sendMessage('AR', {
    method: 'getHTML',
    param: 'myParam'
}, function (response) {
    if (chrome.runtime.lastError) {
        alert('ERROR: ' + chrome.runtime.lastError.message);
    } else {
        alert(response.data);
    }
});
*/
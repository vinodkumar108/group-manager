//Controller for Message list page
app.controller('message_list', function ($scope, $http, config) {
    
	$http.get(config.apiUrl+"Message/message_list")
        .then(function (response) {
            $scope.messages = response.data.message_list;
    });
		
	$http.get(config.apiUrl+"Message/label_list")
        .then(function (response) {
            $scope.labels = response.data.label_list;
    });
		
	// Search post page 
	$scope.searchPost = function()
	{
		var label = $scope.label_name;
		
		console.log("Label search"+label);
		$http.get(config.apiUrl+"Message/message_search/"+label)
        .then(function (response) {
            $scope.messages = response.data.message_list;
        });
	};
	
	//method for open post url
	$scope.openPostUrl = function(postId)
	{
		console.log('Opening url:'+postId);
		
		chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
		chrome.tabs.sendMessage(tabs[0].id, { action: "OPENPOSTURL",post_id:postId }, function (response) {
			
			if (chrome.runtime.lastError) {
				console.log('ERROR1: ' + chrome.runtime.lastError.message);
			} else {
				console.log(response.msg);
				
			}
			 return true; //for async
		
			});
	
		});
	};
		
});

//Controller message Edit
app.controller('message_edit', function ($scope, $http, config, gmService) {
    var param = document.location.search;
    var id = param.substring(param.lastIndexOf("=") + 1);
    console.log(id);
	
	
    $http.get(config.apiUrl+"Message/message_detail/" + id)
        .then(function (response) {
	
			$scope.group_id = response.data.message_id;
			$scope.message_desc = response.data.message_desc;
			$scope.tag_name = response.data.tag_name;			
        });
	
	
    //update
    $scope.submitForm = function () {
        console.log("posting data....");
        var data = {
            message_id: $scope.message_id,
            tag_name: $scope.tag_name,
            message_desc: $scope.message_desc			
        };

        console.log(data);
        console.log(JSON.stringify(data));
        $http.post(config.apiUrl+'Message/message_save/', JSON.stringify(data))
            .success(function (data) {
                console.log("done data posting:");
                console.log(data);
                // Show the Modal on load
                $("#myModal").modal("show");
            });
			
		gmService.addActivity('Message edited successfully.');
    };

    //delete
    $scope.delete = function () {
        $http.get(config.apiUrl+"Message/message_delete/" + id)
            .then(function (response) {
                $("#myModalDelete").modal("show");
            });
		gmService.addActivity('Message deleted successfully.');
    };
});

//move to delete > ok > move to list again
$('#myModalDelete').on('hidden.bs.modal', function () {
  window.location.href=window.location.origin + "/html/post-message.html";
});
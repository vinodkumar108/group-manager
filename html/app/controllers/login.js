	
app.controller('login', function ($scope, $http, config) {
	console.log(config.apiUrl);
	/*
    $http.get(config.apiUrl+"Activity/activity_list")
        .then(function (response) {
			console.log(response.data.activity_list);
            $scope.actvities = response.data.activity_list;			
        });
	*/
	

	var profile_id = localStorage.getItem('profile_id');
	if(profile_id != '')
	{
		window.location= "index.html";
	}
	
	$scope.logOut = function()
	{
		localStorage.setItem('profile_id','');
		window.location= "login.html";
	}

	$scope.loginSubmit = function()
	{
		//console.log('login button click');
		
		var data = {
            email: $scope.email,
            password: $scope.password
        };
		console.log(JSON.stringify(data));

        
        $http.post(config.apiUrl+'Profile/login_user/', JSON.stringify(data))
            .success(function (data) {
                //console.log("done data posting:");
                console.log(data);
				if(data.profile_id != null)
				{
					console.log('Login Success');					
					localStorage.setItem('profile_id',data.profile_id);
					window.location= "index.html";
				}
				else{
					alert('Invalid user Id/password');
				}
                // Show the Modal on load
                //$("#myModal").modal("show");
            });

		//gmService.addActivity('Sweeping edited successfully.');
	};
});


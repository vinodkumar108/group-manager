//Comment listing controller
app.controller('comment_list', function ($scope, $http, config) {
    $http.get(config.apiUrl+"PredefineComment/comment_list")
        .then(function (response) {
            $scope.comments = response.data.comment_list;
			
			
			$scope.addReplyButton($scope.comments);
        });
	
	//method for add reply to button
	$scope.addReplyButton = function(commentList)
	{
		////group_mall_1559224377622806
		//_4-u2 mbm _5jmm _5pat _5v3q _4-u8
		var grp_id = 'group_mall_1559224377622806';
		chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
		chrome.tabs.sendMessage(tabs[0].id, { action: "CannedReply",feedId:grp_id,token:config.fbToken,comments:commentList }, function (response) {
			
			if (chrome.runtime.lastError) {
				console.log('ERROR1: ' + chrome.runtime.lastError.message);
			} else {
				console.log(response.msg);
				//$scope.getPostFeed(uid, UNameVal);
			}
			 return true; //for async
		
			});
		
		});
	};
	
	//Method for canned response
	$scope.cannedResponse = function(postId,commentTxt)
	{
		console.log('Post Id with comments:'+postId);
		$scope.getAllComments('1559224377622806_1825003261044915',commentTxt);
		//$scope.getPageToken('1559224377622806_1798480963697145');	
			
	};
	
	//Method for getting all comments
	$scope.getAllComments = function(postId,commentTxt)
	{
		
		$http({
				url: "https://graph.facebook.com/v2.8/"+postId+"/comments?access_token="+config.fbToken,
				method: "GET"
			}).success(function(data, status, headers, config) {
				//$scope.data = data;
				console.log(data.data[0].id);
				$scope.replyComments(data.data[0].id,commentTxt);
				
			}).error(function(data, status, headers, config) {
				$scope.status = status;
				console.log('error:'+data);
			});
		
	};
	
	
	//Method for reply comments
	$scope.replyComments = function(commentId,commentTxt) 
	{
			$http({
				url: "https://graph.facebook.com/v2.8/"+commentId+"/comments?access_token="+config.fbToken,
				method: "POST",
				data: {"message":commentTxt}
			}).success(function(data, status, headers, config) {
				$scope.data = data;
				console.log('success:'+data);
			}).error(function(data, status, headers, config) {
				$scope.status = status;
				console.log('error:'+data);
			});
	};
	
	//Method for getting page token
	$scope.getPageToken = function(commentId) 
	{
			$http({
				url: "https://graph.facebook.com/v2.8/"+commentId+"?access_token="+config.fbToken,
				method: "POST",
				data: {"message":"Test Reply"}
			}).success(function(data, status, headers, config) {
				$scope.data = data;
				console.log(data);
			}).error(function(data, status, headers, config) {
				$scope.status = status;
				console.log('error:'+data);
			});
	};
		
});

//Controller for comment edit
app.controller('comment_edit', function ($scope, $http, config, gmService) {
    var param = document.location.search;
    var id = param.substring(param.lastIndexOf("=") + 1);
    console.log(id);
    $http.get(config.apiUrl+"PredefineComment/comment_detail/" + id)
        .then(function (response) {
			console.log(response.data.predefined_comment_id);
            $scope.predefined_comment_id = response.data.predefined_comment_id;
            $scope.post_id = response.data.post_id;
            $scope.post_name = response.data.post_name;
			$scope.comment_text = response.data.comment_text;
			 
			$scope.tag_group_id = response.data.tag_group_id;
            $scope.image_path = response.data.image_path;
			$scope.comment_link = response.data.comment_link;
        });

    //update
    $scope.submitForm = function () {
        console.log("posting data....");
        var data = {
            predefined_comment_id:  $scope.predefined_comment_id,
            post_id: $scope.post_id,
            post_name: $scope.post_name,
			comment_text: $scope.comment_text,
			
			tag_group_id: $scope.tag_group_id,
            image_path: $scope.image_path,
			comment_link: $scope.comment_link,
			
        };

        console.log(data);
        console.log(JSON.stringify(data));

        $http.post(config.apiUrl+'PredefineComment/comment_save/', JSON.stringify(data))
            .success(function (data) {
                console.log("done data posting:");
                console.log(data);
                // Show the Modal on load
                $("#myModal").modal("show");
            });
			
		gmService.addActivity('Predefined comment edited successfully.');

    };

    //delete
    $scope.delete = function () {
        $http.get(config.apiUrl+"PredefineComment/comment_delete/" + id)
            .then(function (response) {
                $("#myModalDelete").modal("show");
            });
			
		gmService.addActivity('Predefined comment deleted successfully.');
    };
});

//move to delete > ok > move to list again
$('#myModalDelete').on('hidden.bs.modal', function () {
  window.location.href=window.location.origin + "/html/canned-response.html";
});
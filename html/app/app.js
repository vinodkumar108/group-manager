var app = angular.module('gmapp', [])
    .config([
        '$compileProvider',
        function ($compileProvider) {
            $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension):/);
        }
    ]);
	
app.constant('config', {
    appName: 'GM App',
    appVersion: '1.0',
    apiUrl: 'http://fb-tools.zevik.net/groupmanager-api/index.php/',
	fbToken:'EAACEdEose0cBAMQie1Q6qlOqG3Yu00Mmgkuc94ZB69hEniKZCyenyddMQZC3S4ZCzXqpFPWvVf5M8oZC7GXH3BgJ3ZCPhr7xH32eUIWDOmHQh2wZAAHCSRSu8ibCZBD8uYx9jUnMUKseUWw2L9cMx8Pk9HChXzwn0rb3ro4XtuYeAauqcYWbTN6aHZBiABdeTolkZD'
});


 app.factory('gmService', function($http, config) {
        return {
            addActivity: function(act_desc) {
				
                console.log("posting data....");
        var data = {
            activity_desc: act_desc			
        };

        console.log(data);
        console.log(JSON.stringify(data));

		 $http.post(config.apiUrl+'Activity/activity_save/', JSON.stringify(data))
            .success(function (data) {
                console.log("done data posting:");
                console.log(data);
                // Show the Modal on load
                $("#myModal").modal("show");
            });
            }
        };
    });
	
function logOut()
{
	localStorage.setItem('profile_id','');
	alert('We are log out.');
}
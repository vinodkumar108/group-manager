var app = angular.module('gmapp', [])
    .config([
        '$compileProvider',
        function ($compileProvider) {
            $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension):/);
        }
    ]);
	
app.constant('config', {
    appName: 'GM App',
    appVersion: '1.0',
    apiUrl: 'http://localhost/projects/groupmanager-api/index.php/'
});


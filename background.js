chrome.runtime.onMessage.addListener(
  function (request, sender, sendResponse) {
    
	// console.log(sender.tab ?
    //             "from a content script:" + sender.tab.url :
    //             "from the extension");
	
	var fbToken = "EAACEdEose0cBAMQie1Q6qlOqG3Yu00Mmgkuc94ZB69hEniKZCyenyddMQZC3S4ZCzXqpFPWvVf5M8oZC7GXH3BgJ3ZCPhr7xH32eUIWDOmHQh2wZAAHCSRSu8ibCZBD8uYx9jUnMUKseUWw2L9cMx8Pk9HChXzwn0rb3ro4XtuYeAauqcYWbTN6aHZBiABdeTolkZD";
	
	//This action is called for apply button
	if(request.action == 'applybutton')
	{
		$.ajax({
			type: 'GET',
			url: "http://fb-tools.zevik.net/groupmanager-api/index.php/PredefineComment/comment_list",		
			success: function(data) {			
				sendResponse({ resData: JSON.parse(data),token:fbToken});
			}
		});
	}
	
	//This action is for adding new label to post
	if(request.action == 'addlabel')
	{
	  
	  $.ajax({
			type: 'POST',
			url: "http://fb-tools.zevik.net/groupmanager-api/index.php/Message/message_save",
			data: {"message_desc":request.postdata,"tag_name" : request.label,"post_id" : request.postId,"label_id":request.labelId},
			success: function(data) {
				sendResponse({ message: "Label is saved:"+request.label+":"+request.postdata,token:fbToken});			
			},
			error: function(textStatus, errorThrown) {
				sendResponse({ message: textStatus,token:fbToken});			
			}
		});
		
		//sendResponse({ message: "Label is saved:"+request.label+":"+request.postdata,token:fbToken});			
	}
	
	if(request.action == 'deletelabel')
	{
	  
	  $.ajax({
			type: 'POST',
			url: "http://fb-tools.zevik.net/groupmanager-api/index.php/Message/message_label_delete",
			data: {"post_id" : request.postId,"label_id":request.labelId},
			success: function(data) {
				sendResponse({ message: "Label is deleted",token:fbToken});			
			},
			error: function(textStatus, errorThrown) {
				sendResponse({ message: textStatus,token:fbToken});			
			}
		});
		
		//sendResponse({ message: "Label is saved:"+request.label+":"+request.postdata,token:fbToken});			
	}
	
	if(request.action == 'getlabels')
	{
		
	  $.ajax({
			type: 'GET',
			url: "http://fb-tools.zevik.net/groupmanager-api/index.php/Message/post_labels/"+request.postId,
			data: {"post_id" : request.postId},
			success: function(data) {
				sendResponse({ message: JSON.parse(data),token:fbToken});			
			},
			error: function(textStatus, errorThrown) {
				sendResponse({ message: textStatus,token:fbToken});			
			}
		});
		
		//sendResponse({ message: "Label is saved:"+request.label+":"+request.postdata,token:fbToken});			
	}
	
	
	if(request.action == 'addprofilelist')
	{
		
	 
	  $.ajax({
			type: 'POST',
			url: "http://fb-tools.zevik.net/groupmanager-api/index.php/Profilelist/ext_profile_save",
			data: {"memberList":request.memberList,"profileListName" : request.profileListName},
			success: function(data) {
				sendResponse({ message: data,token:fbToken});			
			},
			error: function(textStatus, errorThrown) {
				sendResponse({ message: textStatus,token:fbToken});			
			}
		});
		
		//sendResponse({ message: request.memberList[0]});			
	}
	
	//Tell Chrome that you are running asynchronous job
	return true; 
	
});


chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
  //console.log('in backgroup js 1');
  chrome.runtime.sendMessage({ action: "helloB" }, function (response) {
    //console.log('in backgroup js 2');
    //console.log(response.farewell);
  });
});
